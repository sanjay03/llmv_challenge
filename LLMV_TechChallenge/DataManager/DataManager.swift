//
//  DataManager.swift
//  LLMV_TechChallenge
//
//  Created by Sanjay Kumar on 23/07/19.
//  Copyright © 2019 Sanjay Kumar. All rights reserved.
//

import Foundation
import CoreData

class DataManager {
    enum FetchMode {
        case refresh
        case loadMoreData
    }
    
    private let pageSize = 20
    static let instance = DataManager()
    var data = [Delivery]()
    var hasNextPage = true
    var isFetching = false
    
    func fetch(_ mode: FetchMode, loadFromCacheWhenFailed: Bool = false) {
        if isFetching { return }
        isFetching = true
        
        let offset = mode == .refresh ? 0 : data.count
        
        APIManager.instance.getDeliveries(offset: offset, limit: pageSize) { (result) in
            if let result = result {
                let newData = result as! [Delivery]
                
                switch mode {
                case .refresh:
                    DatabaseController.deleteAllData()
                    self.data = newData
                    self.hasNextPage = true
                case .loadMoreData:
                    self.data.append(contentsOf: newData)
                    self.hasNextPage = newData.count >= self.pageSize
                    break
                }
              DatabaseController.saveDataOffline(data: newData)
              NotificationCenter.default.post(name: Constants.deliveriesDidUpdate, object: nil)
            } else {
                switch mode {
                case .refresh:
                    if loadFromCacheWhenFailed{
                        let result = DatabaseController.retrieveData()
                        self.data = result
                        NotificationCenter.default.post(name: Constants.deliveriesDidUpdate, object: nil)
                    } else {
                        self.data.removeAll()
                    }
                    break
                case .loadMoreData:
                    break
                }
               
            }
            self.isFetching = false
        }
    }
    
}
