//
//  Delivery.swift
//  LLMV_TechChallenge
//
//  Created by Sanjay Kumar on 18/07/19.
//  Copyright © 2019 Sanjay Kumar. All rights reserved.
//

import Foundation

/// This model contains the properties about the delivery.
struct Delivery: Codable {

    var id: Int?
    var description: String?
    var imageUrl: String?
    var location: Location
   
    
    private enum CodingKeys: String, CodingKey {
        case id
        case description
        case imageUrl
        case location
        
    }
}

struct Location: Codable {
    var lat: Double?
    var lng: Double?
    var address: String?
    
    private enum CodingKeys: String, CodingKey {
        case lat
        case lng
        case address
        
    }
}
