//
//  Constants.swift
//  LLMV_TechChallenge
//
//  Created by Sanjay Kumar on 22/07/19.
//  Copyright © 2019 Sanjay Kumar. All rights reserved.
//

import Foundation

/// This class contains all the constants related to vive app.
class Constants {
    
    private init() { }
    
    static let emptyString = ""
    static let baseURL = "https://mock-api-mobile.dev.lalamove.com/deliveries"
    static let entityName = "Delivery"
    static let cellIdentifier = "MyCell"
    static let googleMapKey = "AIzaSyAWufWVdwSy8X2XQAcwl1Ve06qVhOaKbtE"
    static let deliveriesDidUpdate = Notification.Name("llmv.notification.deliveriesDidUpdate")
    
}
