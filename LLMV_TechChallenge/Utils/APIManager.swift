//
//  APIManager.swift
//  LLMV_TechChallenge
//
//  Created by Sanjay Kumar on 23/07/19.
//  Copyright © 2019 Sanjay Kumar. All rights reserved.
//

import Alamofire

class APIManager {
    
    static let instance = APIManager()
    private let url = Constants.baseURL
    
    //MARK: -  Get Delivery List
    func getDeliveries(offset: Int, limit: Int, completion: @escaping (Any?) -> Void) {
        let parameters = ["offset": offset, "limit": limit]
        
        AF.request(Constants.baseURL, method: .get, parameters: parameters,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            
            switch response.result {
            case .success:
                print(response)
                guard let data = response.data else { return }
                do {
                    let decoder = JSONDecoder()
                    let gitData = try decoder.decode([Delivery].self, from: data)
                    completion(gitData)
                    
                }catch let err {
                    print("Err", err)
                }
                break
            case .failure(let error):
                completion(nil)
                print(error)
            }
        }
    }
}
