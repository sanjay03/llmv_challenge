//
//  ControllerBase.swift
//  LLMV_TechChallenge
//
//  Created by Sanjay Kumar on 22/07/19.
//  Copyright © 2019 Sanjay Kumar. All rights reserved.
//

import UIKit

class ControllerBase: UIViewController {
    
    private var activityIndicator: ActivityIndicator!
    var window: UIWindow?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator = ActivityIndicator(frame: self.view.bounds)
        
    }
    
    // MARK: - Activity indicator show/hide methods.
    
    /**
     Display activity indicator if not visible on screen with text @message.
     
     - Parameters message: The message to be displayed on activity indicator
     */
    func showActivityIndicator(_ message:String = Constants.emptyString) {
        
        if activityIndicator.superview == nil {
            self.view.addSubview(activityIndicator)
        }
        
        activityIndicator.startAnimating(text: message)
    }
    
    /// Hides the activity indicator from the screen.
    func hideActivityIndicator() {
        
        activityIndicator.stopAnimating()
        activityIndicator.removeFromSuperview()
    }
}

