//
//  ActivityIndicator.swift
//  LLMV_TechChallenge
//
//  Created by Sanjay Kumar on 22/07/19.
//  Copyright © 2019 Sanjay Kumar. All rights reserved.
//

import UIKit

/// Used to display activity indicator on screen.
/// Its an instance based class.
class ActivityIndicator: UIView {
    
    private let statusLabel: UILabel
    private let indicatorView: UIActivityIndicatorView
    
    private let statusLabelHeight: CGFloat = 80.0
    private let gap: CGFloat = 5.0
    
    override init(frame: CGRect) {
        
        indicatorView = UIActivityIndicatorView(style: .whiteLarge)
        indicatorView.hidesWhenStopped = true
        indicatorView.backgroundColor = .clear
        
        statusLabel = UILabel(frame: .zero)
        statusLabel.numberOfLines = 5
        statusLabel.font = .boldSystemFont(ofSize: 17.5)
        statusLabel.textAlignment = .center
        statusLabel.textColor = .white
        statusLabel.backgroundColor = .clear
        
        super.init(frame: frame)
        
        self.backgroundColor = .black
        self.alpha = 0.3
        
        self.addSubview(indicatorView)
        self.addSubview(statusLabel)
        
        repositionActivityIndicator(frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        indicatorView = UIActivityIndicatorView(style: .whiteLarge)
        indicatorView.hidesWhenStopped = true
        indicatorView.backgroundColor = .clear
        
        statusLabel = UILabel(frame: .zero)
        statusLabel.numberOfLines = 5
        statusLabel.font = .boldSystemFont(ofSize: 17.5)
        statusLabel.textAlignment = .center
        statusLabel.textColor = .white
        statusLabel.backgroundColor = .clear
        
        super.init(coder: aDecoder)
    }
    
    // MARK: - Private method
    
    private func repositionActivityIndicator(_ backViewFrame: CGRect) {
        
        self.frame = backViewFrame
        
        indicatorView.frame = CGRect(x: (backViewFrame.size.width - indicatorView.frame.size.width)/2, y: (backViewFrame.size.height - indicatorView.frame.size.height)/2 - statusLabelHeight, width: indicatorView.frame.size.width, height: indicatorView.frame.size.height)
        statusLabel.frame = CGRect(x: 0, y: indicatorView.frame.origin.y + indicatorView.frame.size.height + gap, width: backViewFrame.size.width, height: statusLabelHeight)
    }
    
    // MARK: - Public method
    
    /// Set the statusLabel text and start animating spinner
    ///
    /// - Parameter text: The text to be shown with activity indicator
    func startAnimating(text: String? = nil) {
        indicatorView.startAnimating()
        statusLabel.text = text
    }
    
    /// Stops animating spinner
    func stopAnimating() {
        indicatorView.stopAnimating()
    }
}


