//
//  DataBaseController.swift
//  LLMV_TechChallenge
//
//  Created by Sanjay Kumar on 19/07/19.
//  Copyright © 2019 Sanjay Kumar. All rights reserved.
//

import UIKit
import CoreData
import Kingfisher

class DatabaseController{
    
    
    
    // MARK: - Core Data stack
    
    class func getContext() -> NSManagedObjectContext {
        return self.persistentContainer.viewContext
    }
    
    static var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "LLMV_TechChallenge")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    class func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    class func saveDataOffline(data: [Delivery]) {
       
        for deliveryData in data {
            
            let deliveryEntity = NSEntityDescription.entity(forEntityName: Constants.entityName, in: DatabaseController.getContext())
            
            let delivery = NSManagedObject(entity: deliveryEntity!, insertInto: DatabaseController.getContext())
            
            
            delivery.setValue(deliveryData.id, forKey: "id")
            delivery.setValue(deliveryData.description, forKey: "descriptions")
            
            let locationData = deliveryData.location
            delivery.setValue(locationData.address, forKey: "address")
            delivery.setValue(locationData.lat, forKey: "lat")
            delivery.setValue(locationData.lng, forKey: "lng")
            
            let imageView:UIImageView = UIImageView()
            let imageURL = URL(string: deliveryData.imageUrl!)
            imageView.kf.setImage(with: imageURL){ result in
                switch result {
                case .success:
                    delivery.setValue(imageView.image?.pngData(), forKey: "imageData")
                case .failure(let error):
                    print("Error: \(error)")
                }
          
            DatabaseController.saveContext()
        }
        
        }
        
    }
    
    class func retrieveData() -> [Delivery] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Constants.entityName)
        fetchRequest.returnsObjectsAsFaults = false
        var deliveryDataArray = [Delivery]()
        do{
            let results = try DatabaseController.getContext().fetch(fetchRequest)
            for data in results as! [NSManagedObject] {
             
                let location = Location(lat: data.value(forKey: "lat") as? Double,
                                        lng: data.value(forKey: "lng") as? Double,
                                        address: data.value(forKey: "address") as? String)
                
                let imageData = data.value(forKey: "imageData") as! Data
                let dataString = String(decoding: imageData, as: UTF8.self)
                
                let imageView:UIImageView = UIImageView()
                let imageDataNew = Data(dataString.utf8)
                imageView.image = UIImage(data: imageDataNew)
                
                let deliveryData = Delivery(id: data.value(forKey: "id") as? Int,
                                            description: data.value(forKey: "descriptions") as? String,
                                            imageUrl: dataString as String?,
                                            location: location)
                
                deliveryDataArray.append(deliveryData)
                
            }
            print(results.count)
            
        } catch {
            print(error)
        }
        return deliveryDataArray
    }
    
    class func deleteAllData() {

        let context = DatabaseController.getContext()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Constants.entityName)
        fetchRequest.entity = NSEntityDescription.entity(forEntityName: Constants.entityName, in: context)
        fetchRequest.includesPropertyValues = false

        if let results = try? context.fetch(fetchRequest) as? [NSManagedObject] {
            for result in results {
               context.delete(result)
            }

           try? context.save()
        }
    }
}

