//
//  DeliveryDetailController.swift
//  LLMV_TechChallenge
//
//  Created by Sanjay Kumar on 18/07/19.
//  Copyright © 2019 Sanjay Kumar. All rights reserved.
//

import UIKit
import Alamofire
import CoreData
import Kingfisher
import Cartography

class DeliveryViewController: ControllerBase, UITableViewDelegate,UITableViewDataSource {
    
    private lazy var photoView: UIImageView = UIImageView()
    fileprivate var loadingFooter: UIActivityIndicatorView!

    private var tableView: UITableView!
    private let refreshControl = UIRefreshControl()
    
    let reachability = NetworkReachabilityManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(deliveriesDidUpdate), name: Constants.deliveriesDidUpdate, object: nil)
     
        setupTableView()
        DataManager.instance.fetch(.refresh, loadFromCacheWhenFailed: true)
    }
    
    // MARK: - View Methods
    
    private func setupTableView() {
        title = "Things to deliver"
        let barHeight: CGFloat = UIApplication.shared.statusBarFrame.size.height
        let displayWidth: CGFloat = self.view.frame.width
        let displayHeight: CGFloat = self.view.frame.height
        
        tableView = UITableView(frame: CGRect(x: 0, y: 0, width: displayWidth, height: displayHeight - barHeight))
        tableView.register(DeliveryCell.classForCoder(), forCellReuseIdentifier: "DeliveryCell")
        tableView.backgroundColor = UIColor(white: 0.97, alpha: 1.0)
        tableView.rowHeight = 90
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.footerView(forSection: .zero)
        
        view.backgroundColor = UIColor(white: 0.97, alpha: 1.0)
        view.addSubview(tableView)
        constrain(view, tableView) { (view, tableView) -> () in
            tableView.edges == inset(view.edges, 0, 0, 0, 0)
        }
        
        loadingFooter = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44))
        loadingFooter.color = .black
        loadingFooter.startAnimating()
        
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        
        // Configure Refresh Control
        refreshControl.addTarget(self, action: #selector(fetchDeliveryData(_:)), for: .valueChanged)
    }
    
    @objc func updateView() {
      
        tableView.reloadData()
        
    }

    // MARK: - Helper Methods
    @objc private func fetchDeliveryData(_ sender: Any) {
        
        DataManager.instance.fetch(.refresh, loadFromCacheWhenFailed: true )
    }
    
    @objc func deliveriesDidUpdate() {
        //Stop the pullToRefrsh
        tableView.refreshControl?.endRefreshing()
   
        //Reload TableView
        tableView.reloadData()
        tableView.tableFooterView = nil
    }
    
    // MARK: - TableView Methods
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let delivery = DataManager.instance.data[indexPath.row]
        let vc = DeliveryDetailController(delivery)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataManager.instance.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DeliveryCell", for: indexPath) as! DeliveryCell
        cell.delivery = DataManager.instance.data[indexPath.row]
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentSize.height > 0 && scrollView.contentOffset.y > scrollView.contentSize.height - scrollView.bounds.height {
            if !DataManager.instance.isFetching && DataManager.instance.hasNextPage {
                tableView.tableFooterView = loadingFooter
                DataManager.instance.fetch(.loadMoreData)
            }
        }
    }
    
}

