//
//  DeliveryViewController.swift
//  LLMV_TechChallenge
//
//  Created by Sanjay Kumar on 18/07/19.
//  Copyright © 2019 Sanjay Kumar. All rights reserved.
//

import UIKit
import GoogleMaps
import Kingfisher
import Cartography

class DeliveryDetailController: ControllerBase {
    
    lazy var cellImageView: CellImageView = CellImageView(self.delivery, showBorder: true)
    private var mapView: GMSMapView!
    var imageView:UIImageView?
    private var delivery: Delivery!
    
    convenience init(_ delivery: Delivery) {
        self.init()
        self.delivery = delivery
    }
    
    //MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        addViews()
    }
    
     // MARK: - View Methods
    
    func addViews() {
 
        let camera = GMSCameraPosition.camera(withLatitude: delivery.location.lat!, longitude: delivery.location.lng!, zoom: 15)
      
        mapView = GMSMapView.map(withFrame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 400), camera: camera)
     
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: delivery.location.lat!, longitude: delivery.location.lng!)
        marker.title = delivery.location.address
        marker.map = mapView
        
        title = "Delivery Detail"
        view.backgroundColor = .white
        
        // Add components to StackView, from to to bottom.
        view.addSubview(mapView)
        
        view.addSubview(cellImageView)
        
        // AutoLayout
        constrain(view, mapView, cellImageView) { (view, mapView, cellImageView) -> () in
            mapView.edges == inset(view.edges, 0, 0, 0, 0)
            cellImageView.left == view.left + 24
            cellImageView.right == view.right - 24
            cellImageView.bottom == view.bottom - 24
            cellImageView.height == 80
        }
    }
}
