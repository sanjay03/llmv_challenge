//
//  DeliveryCell.swift
//  LLMV_TechChallenge
//
//  Created by Sanjay Kumar on 23/07/19.
//  Copyright © 2019 Sanjay Kumar. All rights reserved.
//

import Cartography

final class DeliveryCell: UITableViewCell {
    // View Components
    lazy var cellImageView: CellImageView = self.makeCellImageView()
    
    // Update content when set delivery
    var delivery: Delivery? {
        didSet {
            cellImageView.delivery = delivery
            cellImageView.isHidden = false
        }
    }
    
    //MARK: - Initialize
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        drawUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func drawUI() {
        backgroundColor = .clear
        contentView.addSubview(cellImageView)
        constrain(contentView, cellImageView) { (contentView, cellImageView) -> () in
            cellImageView.edges == inset(contentView.edges, 12, 12, 0, 12)
        }
    }
 
    // MARK: - Override functions
    override func setSelected(_ selected: Bool, animated: Bool) {
        // Override this function to prevent background color change when selected
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        // Change color when highlight
        cellImageView.backgroundColor = highlighted ? UIColor(white: 0.94, alpha: 1.0) : .white
    }
}

//MARK: - Make View Components
extension DeliveryCell {
    fileprivate func makeCellImageView() -> CellImageView {
        let cellImageView = CellImageView(nil)
        cellImageView.layer.shadowRadius = 1
        cellImageView.layer.shadowColor = UIColor.gray.cgColor
        cellImageView.layer.shadowOpacity = 0.5
        cellImageView.layer.shadowOffset = CGSize(width: 1, height: 1)
        return cellImageView
    }
}

