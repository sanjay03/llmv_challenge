//
//  CellImageView.swift
//  LLMV_TechChallenge
//
//  Created by Sanjay Kumar on 23/07/19.
//  Copyright © 2019 Sanjay Kumar. All rights reserved.
//

import Kingfisher
import Cartography
import SwiftIcons
import Alamofire

class CellImageView: UIView {
  
    private lazy var imageView: UIImageView = UIImageView()
    private lazy var titleLabel: UILabel = UILabel()
    private lazy var subtitleLabel: UILabel = UILabel()
    
    var delivery: Delivery? {
        didSet {
            if let delivery = delivery {
                if NetworkReachabilityManager()!.isReachable {
                    let imageURL = URL(string: delivery.imageUrl!)
                    imageView.kf.setImage(with: imageURL)
                }else {
                    let data = Data(delivery.imageUrl!.utf8)
                    imageView.image = UIImage(data: data)
                }
                
                titleLabel.text = delivery.description
                subtitleLabel.text = delivery.location.address
            } else {
                imageView.kf.cancelDownloadTask()
                imageView.image = nil
                titleLabel.text = nil
                subtitleLabel.text = nil
            }
        }
    }
    
    //MARK: - Init with delivery object
    convenience init(_ delivery: Delivery?, showBorder: Bool = false) {
        self.init()
        drawUIComponents()
        if showBorder {
            layer.borderWidth = max(1 / UIScreen.main.scale, 0.5)
            layer.borderColor = UIColor.lightGray.cgColor
        }
        setDelivery(delivery)
    }
    
    private func setDelivery(_ delivery: Delivery?) {
        self.delivery = delivery
    }
    
    //MARK: - Draw UI Components
    private func drawUIComponents() {
        backgroundColor = .white
     
        let textStackView = UIStackView(arrangedSubviews: [titleLabel, subtitleLabel])
        textStackView.axis = .vertical
        textStackView.distribution = .equalSpacing
        textStackView.alignment = .leading
        textStackView.spacing = 4
    
        let stackView = UIStackView(arrangedSubviews: [imageView, textStackView])
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.alignment = .center
        stackView.spacing = 8
        addSubview(stackView)
        
        // AutoLayout
        constrain(self, imageView, stackView) { (view, photoView, stackView) -> () in
            stackView.edges == inset(view.edges, 8, 8, 8, 8)
            photoView.width == photoView.height
        }
        
    }
}
