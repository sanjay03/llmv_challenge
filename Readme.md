
## Table of contents
* [General info]
* [Technologies]
* [Setup]
* [Deployment]
* [Dependencies]

## General info
This project is based on fetching delivery data from API and show the list of delivery to user.

## Technologies
Project is created with:
* Xcode version: 10.2.1
* Language: Swift 5
* Programming paradigm: Object oriented
* Local data storage: Core Data
* Architecture pattern: MVVM

## Setup
To run this project, install Xcode then open this project:

## Deployment 
* Device: iPhone

## Dependencies 
* Frameworks Used: Alamofire, GoogleMaps, SwiftLint, Kingfisher, SwiftIcons, Cartography

